package addtwonumbers

type ListNode struct {
	Val  int
	Next *ListNode
}

func addTwoNumbers(l1 *ListNode, l2 *ListNode) *ListNode {
	dummyHead := new(ListNode) // 哨兵节点
	cur := dummyHead           // 不在此节点操作，结果返回dummy.Next使用
	carry := 0
	for l1 != nil || l2 != nil || carry > 0 {
		cur.Next = new(ListNode) // 新建一个节点存储计算结果（操作真实头节点）
		cur = cur.Next
		if l1 != nil {
			carry += l1.Val
			l1 = l1.Next
		}
		if l2 != nil {
			carry += l2.Val
			l2 = l2.Next
		}
		cur.Val = carry % 10
		carry /= 10
	}
	return dummyHead.Next
}
