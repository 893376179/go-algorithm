package lengthOfLongestSubstring

import "strings"

func lengthOfLongestSubstring1(s string) int {
	max := 1
	res := 0
	for i := 0; i < len(s); i++ {
		max = 1
		for j := i + 1; j < len(s); j++ {
			temp := s[i:j]
			if strings.Index(temp, string(s[j])) != -1 {
				break
			}
			max++
		}
		if res < max {
			res = max
		}
	}
	return res
}

// 利用语言特性，直接将字符转换成int，用一个128位的int数组存储字符下标
func lengthOfLongestSubstring2(s string) int {
	res := 0
	cmap := make([]int, 128)
	left := 0
	for right := 0; right < len(s); right++ {
		index := s[right]
		left = max(left, cmap[index])
		res = max(res, right-left+1)
		cmap[index] = right + 1
	}
	return res
}

func max(a, b int) int {
	if a > b {
		return a
	} else {
		return b
	}
}
