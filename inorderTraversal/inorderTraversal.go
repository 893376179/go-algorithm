package inorderTraversal

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func inorderTraversal(root *TreeNode) (res []int) {
	var traversal func(node *TreeNode)
	traversal = func(node *TreeNode) {
		if node == nil {
			return
		}
		traversal(node.Left)
		res = append(res, node.Val)
		traversal(node.Right)
	}
	traversal(root)
	return res
}

// 或者拆分成两个函数
func inorderTraversal2(root *TreeNode) (res []int) {
	inorder(&res, root)
	return res
}

func inorder(res *[]int, root *TreeNode) {
	if root == nil {
		return
	}
	inorder(res, root.Left)
	*res = append(*res, root.Val)
	inorder(res, root.Right)

}
