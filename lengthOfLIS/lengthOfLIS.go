package lengthOfLIS

import "sort"

/* 1     7     2     5     6     4     3       // 原序列
   1     1     1     1     1     1     1       // dp初始化
   1  1+1=2  1+1=2  1+2=3  1+3=4 1+2=3 1+2=3   // 加上在当前位置之前且小于当前值的数值
最终返回dp中最大的值，即为最大长度
*/
func lengthOfLIS1(nums []int) int {
	if len(nums) == 1 {
		return 1
	}
	dp := make([]int, len(nums))
	for i := 0; i < len(nums); i++ {
		dp[i] = 1 // 初始化
	}
	res := 1
	for i := 1; i < len(nums); i++ {
		for j := 0; j < i; j++ {
			if nums[i] > nums[j] {
				dp[i] = max(dp[i], dp[j]+1)
			}
		}
		res = max(res, dp[i])
	}
	return res
}

func max(a, b int) int {
	if a > b {
		return a
	} else {
		return b
	}
}

func lengthOfLIS2(nums []int) int {
	f := []int{}
	for _, e := range nums {
		if i := sort.SearchInts(f, e); i < len(f) { // golang内置的SearchInts函数已经实现了二分查找
			f[i] = e
		} else {
			f = append(f, e)
		}
	}
	return len(f)
}
