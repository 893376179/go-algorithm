package reverseListNode

type ListNode struct {
	Val  int
	Next *ListNode
}

func reverseListNode(head *ListNode) *ListNode {
	// 反转后半部分链表，返回反转后的头节点
	var pre, cur *ListNode = nil, head
	for cur != nil {
		nextTmp := cur.Next
		cur.Next = pre
		pre = cur
		cur = nextTmp
	}
	return pre
}

func findFirstEndNode(head *ListNode) *ListNode {
	// 返回前半部分链表的尾节点
	fast := head
	slow := head
	for fast.Next != nil && fast.Next.Next != nil {
		fast = fast.Next.Next
		slow = slow.Next
	}
	return slow
}

func isPalindrome(head *ListNode) bool {
	if head == nil {
		return true
	}
	firstEndNode := findFirstEndNode(head)
	secondFirstNode := reverseListNode(firstEndNode.Next)

	p1 := head
	p2 := secondFirstNode

	for p2 != nil {
		if p1.Val != p2.Val {
			return false
		}
		p1 = p1.Next
		p2 = p2.Next
	}
	// 还原链表
	firstEndNode.Next = reverseListNode(secondFirstNode)
	return true

}
