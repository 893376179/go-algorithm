package moveZeroes

func moveZeroes1(nums []int) {
	j, length := 0, len(nums)
	for i := 0; i < length; i++ {
		if nums[i] != 0 {
			if i != j {
				nums[i], nums[j] = nums[j], nums[i]
			}
			j++
		}
	}
}

func moveZeroes2(nums []int) {
	length := len(nums)
	i := 0
	for {
		if i >= length {
			break
		}
		if nums[i] == 0 {
			length -= 1                             // 长度减1
			nums = append(nums[0:i], nums[i+1:]...) // 遇到0，使用区间前闭后开去除0元素，重整切片
			nums = append(nums, 0)                  // 删除0元素后，末尾补0
		} else {
			i += 1
		}
	}
}
