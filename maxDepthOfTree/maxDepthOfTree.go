package maxdepthoftree

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func maxDepth(root *TreeNode) int {
	res := 0
	var maxdepth func(root *TreeNode, depth int)
	maxdepth = func(root *TreeNode, depth int) {
		if root == nil {
			return
		}
		if root.Left == nil && root.Right == nil {
			res = max(res, depth)
			return
		}
		maxdepth(root.Left, depth+1)
		maxdepth(root.Right, depth+1)
	}
	maxdepth(root, 1)
	return res
}

func max(x, y int) int {
	if x > y {
		return x
	} else {
		return y
	}
}
