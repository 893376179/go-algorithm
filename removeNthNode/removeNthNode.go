package removeNthNode

type ListNode struct {
	Val  int
	Next *ListNode
}

func removeNthNode(head *ListNode, n int) *ListNode {
	dummyHead := &ListNode{ // 防止删除头节点
		Next: head,
	}
	slow, fast := dummyHead, dummyHead
	for i := 0; i < n+1; i++ {
		fast = fast.Next
	}
	for fast != nil {
		fast = fast.Next
		slow = slow.Next
	}
	slow.Next = slow.Next.Next // 删除
	return dummyHead.Next
}
