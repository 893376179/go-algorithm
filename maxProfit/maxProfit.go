package maxProfit

func maxProfit(prices []int) int {
	if len(prices) == 0 {
		return 0
	}
	var min = prices[0]
	var profit int
	for _, price := range prices {
		if price-min > profit {
			profit = price - min
		}
		if price < min {
			min = price
		}
	}
	return profit
}
